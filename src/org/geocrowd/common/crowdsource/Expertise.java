/*******************************************************************************
* @ Year 2013
* This is the source code of the following papers. 
* 
* 1) Geocrowd: A Server-Assigned Crowdsourcing Framework. Hien To, Leyla Kazemi, Cyrus Shahabi.
* 
* 
* Please contact the author Hien To, ubriela@gmail.com if you have any question.
*
* Contributors:
* Hien To - initial implementation
*******************************************************************************/

package org.geocrowd.common.crowdsource;

import java.util.HashSet;


// TODO: Auto-generated Javadoc
/**
 * The Class Expertise.
 */
public class Expertise {
	
	/** The expertise. */
	private HashSet<Integer> expertise = new HashSet<>();

	
	/**
	 * Instantiates a new expertise.
	 */
	public Expertise() {
		super();
	}

}
