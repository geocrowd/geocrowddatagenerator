package org.geocrowd.datasets.dtype;

public enum DataTypeEnum {
	VALUE_FREQ,
	VALUE_LIST,
	NORMAL_POINT,
	WEIGHT_POINT
}
