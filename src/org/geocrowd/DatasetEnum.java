/*******************************************************************************
 * @ Year 2013
 * This is the source code of the following papers. 
 * 
 * 1) Geocrowd: A Server-Assigned Crowdsourcing Framework. Hien To, Leyla Kazemi, Cyrus Shahabi.
 * 
 * 
 * Please contact the author Hien To, ubriela@gmail.com if you have any question.
 *
 * Contributors:
 * Hien To - initial implementation
 *******************************************************************************/
package org.geocrowd;

// TODO: Auto-generated Javadoc
/**
 * The Enum DatasetEnum.
 */
public enum DatasetEnum {

	/** The skewed. */
	SKEWED,
	/** The uniform. */
	UNIFORM,
	/** The gowalla. */
	GOWALLA,
	/** The yelp. */
	YELP,
	/** The small. */
	SMALL_TEST, 
	
	FOURSQUARE
}
