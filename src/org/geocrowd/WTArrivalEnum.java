package org.geocrowd;

public enum WTArrivalEnum {
	CONSTANT,
	DECREASING,
	INCREASING,
	COSINE,
	POISSON,
	ZIPFIAN
}
